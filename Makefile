
CC ?= gcc

CFLAGS := -ggdb -fno-omit-frame-pointer
CFLAGS += -MMD
CFLAGS += -D_GNU_SOURCE
CFLAGS += -Wall -Wextra -Werror -Wformat=2
CLFAGS += -fsanitize=address -fsanitize=undefined

LDFLAGS += -fsanitize=address -fsanitize=undefined

sources := $(shell find src -type f -name '*.c')
objs := $(addprefix obj/,$(patsubst %c,%o,$(sources:src/%=%)))

exe := bin/main

all: $(exe)

obj/%.o: src/%.c Makefile
	@ echo " CC $<"
	@ mkdir -p obj
	@ $(CC) $(CFLAGS) -c $< -o $@

-include $(wildcard obj/*.d)

$(exe) : bin/% : $(objs)
	@ echo " LD $@"
	@ mkdir -p bin
	@ $(CC) $(LDFLAGS) -o $@ $< $(ofiles)

clean:
	@ $(RM) -r obj
	@ $(RM) -r bin
.PHONY: clean

format:
	@ clang-format -i src/*.c src/*.h
.PHONY: format

tidy:
	@ clang-tidy --fix -checks=bugprone-\* src/*.c src/*.h -- $(CFLAGS)
.PHONY: tidy

