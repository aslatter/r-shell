This project doesn't have much point - but I was reading
about an article that talked about reverse-shells, and I
wanted to make sure I understood the concept so I wrote
a basic one.

To build the server:

```sh
make
```

To run the 'server':

```sh
bin/main
```

The connect to the 'server':

```sh
nc -nvl 127.0.0.1 1337
```

Or for a full shell-experience:

```sh
socat -,raw,echo=0 tcp-l:1337
```

